﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SortingAlgorithms;
namespace ComparsionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            System.Diagnostics.Stopwatch time = new System.Diagnostics.Stopwatch();
            bool flag;
            int n,menu;
            double a, b;
            do
            {
                Console.Write("Enter number of array elements: ");
                flag = int.TryParse(Console.ReadLine(), out n);
                if (!flag) Console.WriteLine("Incorrect!Try again!");
            } while (!flag);
            do
            {
                Console.Write("Enter upper random number: ");
                flag = double.TryParse(Console.ReadLine(), out a);
                if (!flag) Console.WriteLine("Incorrect!Try again!");
            } while (!flag);
            do
            {
                Console.Write("Enter lower random number: ");
                flag = double.TryParse(Console.ReadLine(), out b);
                if (!flag) Console.WriteLine("Incorrect!Try again!");
            } while (!flag);
            do
            {
                Console.WriteLine("Choose filling of elements:\n" +
                    "1.All elements are random\n" +
                    "2.All elements are the same\n" +
                    "3.Array is sorted already\n" +
                    "4.Array is inverted sorted");
                flag = int.TryParse(Console.ReadLine(),out menu);
                if (!flag) Console.WriteLine("Incorrect!Try again!");
            } while (!flag);
            
            double[] mas = new double[n];
            double[] tempmas = new double[n];
            
            switch (menu)
            {
                case 1:
                    {
                        mas = FillArray(mas, a, b);
                        tempmas = mas;
                        break;
                    }
                case 2:
                    {
                        mas = FillArraySame(mas);
                        tempmas = mas;
                        break;
                    }
                case 3:
                    {
                        mas = FillArraySorted(mas);
                        tempmas = mas;
                        break;
                    }
                case 4:
                    {
                        mas = FillArrayInvertedSorted(mas);
                        tempmas = mas;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Incorrect!");
                        Environment.Exit(0);
                        break;
                    }
            }
            Sorting.Display(mas);
            Sorting.Display(tempmas);
            Console.Write("Bubble Sorting: ");
            time.Start();
            tempmas = Sorting.Bubble(tempmas);
            time.Stop();
            Console.WriteLine($"{(double)time.ElapsedMilliseconds/1000:F5} sec");
            time.Reset();
            tempmas = mas;
            Sorting.Display(tempmas);
            Console.Write("Selection Sorting: ");
            time.Start();
            tempmas = Sorting.Selection(tempmas);
            time.Stop();
            Console.WriteLine($"{(double)time.ElapsedMilliseconds / 1000:F5} sec");
            time.Reset();
            tempmas = mas;

            Console.Write("Quick Sorting: ");
            time.Start();
            Sorting.QuickSort_Sort(tempmas,0,tempmas.Length-1);
            time.Stop();
            Console.WriteLine($"{(double)time.ElapsedMilliseconds / 1000:F5} sec");
            time.Reset();
            tempmas = mas;

            Console.Write("Insert Sorting: ");
            time.Start();
            tempmas = Sorting.Inserting(tempmas);
            time.Stop();
            Console.WriteLine($"{(double)time.ElapsedMilliseconds / 1000:F5} sec");
            time.Reset();
            tempmas = mas;

            Console.Write("Shell Sorting: ");
            time.Start();
            tempmas = Sorting.Shell(tempmas);
            time.Stop();
            Console.WriteLine($"{(double)time.ElapsedMilliseconds / 1000:F5} sec");
            time.Reset();
            tempmas = mas;

            Console.Write("Merge Sorting: ");
            time.Start();
            tempmas = Sorting.Merge_Sort(tempmas);
            time.Stop();
            Console.WriteLine($"{(double)time.ElapsedMilliseconds / 1000:F5} sec");
            time.Reset();
            
        }
        static double[] FillArray(double[] arr,double a,double b)
        {
            Random rnd = new Random();
            for(int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.NextDouble() * (b - a) + a;
            }
            return arr;
        }
        static double[] FillArraySame(double[] arr)
        {
            
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = 7;
            }
            return arr;
        }
        static double[] FillArraySorted(double[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = i;
            }
            return arr;
        }
        static double[] FillArrayInvertedSorted(double[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = arr.Length - i;
            }
            return arr;
        }

        
    }
}
