﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class Tasks
    {
        public static void FillAndDisplayMatrix(int[,] mas,int a,int b)
        {
            Random rnd = new Random();
            if (mas.GetLength(0) != mas.GetLength(1))
            {
                Console.WriteLine("\nYour Matrix: ");
            }
            else
            {
                Console.WriteLine("Your square Matrix: ");
            }
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    mas[i, j] = rnd.Next(a, b);
                    Console.Write($"{mas[i, j],3} ");
                }
                Console.WriteLine();
            }
        }
        public static void DisplayMatrix <T>(T [,] mas)
        {
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    
                    Console.Write($"{mas[i, j],6:F3} ");
                }
                Console.WriteLine();
            }
        }

        public static void DisplayDoubleIntMatrix(double[,] mas)
        {
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {

                    Console.Write($"{mas[i, j],3:F0} ");
                }
                Console.WriteLine();
            }
        }
        public static void CountOfRowsWithoutNulls(int[,] mas)
        {
            int countrows = 0, countnonull = 0;
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {

                    if (mas[i, j] == 0)
                    {

                        break;
                    }
                    else
                    {
                        countnonull++;
                    }
                    if (countnonull == mas.GetLength(1)) countrows++;
                }
                countnonull = 0;
            }
            Console.WriteLine($"\nCount of rows with no nulls - {countrows}");
        }
        public static void CountOfMaxNumberMoreThanOneTime(int[,] mas)
        {
            int[] more_than_one = new int[mas.Length];
            int tmp = mas[0, 0], count = 0, num = 0;
            for (int k = 0; k < mas.GetLength(0); k++)
            {

                for (int l = 0; l < mas.GetLength(1); l++)
                {
                    tmp = mas[k, l];
                    for (int i = 0; i < mas.GetLength(0); i++)
                    {
                        for (int j = 0; j < mas.GetLength(1); j++)
                        {
                            if (mas[i, j] == tmp)
                            {
                                count++;
                            }
                            if (count == 2)
                            {
                                more_than_one[num] = tmp;
                                num++;
                                break;
                            }
                        }
                        if (count == 2)
                        {
                            break;
                        }
                    }
                    count = 0;
                }
            }
            int max = more_than_one[0];
            for (int i = 0; i < more_than_one.Length; i++)
            {
                if (more_than_one[i] > max)
                {
                    max = more_than_one[i];
                }
            }
            Console.WriteLine($"Max from more than one elements is - {max}");
        }

        public static void CountOfColsWithAtLeastOneNull(int[,] mas)
        {
            int countcols = 0;
            for (int i = 0; i < mas.GetLength(1); i++)
            {
                for (int j = 0; j < mas.GetLength(0); j++)
                {
                    if (mas[j, i] == 0)
                    {
                        countcols++;
                        break;
                    }
                }
            }
            Console.WriteLine($"Count of cols with at least one null = {countcols}");
        }

        public static void NumberOfRowWithTheLongestSerie(int[,] mas)
        {
            int[] lengths_of_series = new int[mas.Length];
            int[] lenghts_of_max_series = new int[mas.GetLength(0)];
            int[] indexes = new int[mas.Length];
            int[] maxindexes = new int[mas.GetLength(0)];
            int tmp = 0, count = 0, length_num = 0, max_serie_in_a_row = 0;


            for (int l = 0; l < mas.GetLength(0); l++)
            {

                for (int i = 0; i < mas.GetLength(1); i++)
                {
                    tmp = mas[l, i];
                    for (int j = i; j < mas.GetLength(1); j++)
                    {
                        if (mas[l, j] == tmp)
                        {
                            count++;
                        }

                        else
                        {
                            lengths_of_series[length_num] = count;
                            indexes[length_num] = l;

                            length_num++;

                            break;
                        }


                    }
                    count = 0;
                }
                max_serie_in_a_row = lengths_of_series[0];
                for (int z = 0; z < lengths_of_series.Length; z++)
                {
                    if (lengths_of_series[z] > max_serie_in_a_row)
                    {
                        max_serie_in_a_row = lengths_of_series[z];
                    }
                }
                lenghts_of_max_series[l] = max_serie_in_a_row;
                maxindexes[l] = indexes[0];
                length_num = 0;
            }

            int result = lenghts_of_max_series[0], resulti = 0;

            for (int i = 0; i < lenghts_of_max_series.Length; i++)
            {
                if (lenghts_of_max_series[i] > result)
                {
                    result = lenghts_of_max_series[i];
                    resulti = i;
                }
            }
            Console.WriteLine($"Index of row with longest serie = {resulti},the length of serie is - {result}");

        }

        public static void MultiplyInRowsWithNoNegativeElements(int[,] mas)
        {
            int[] multiplies = new int[mas.GetLength(0)];
            int[] indexes = new int[mas.GetLength(0)];
            indexes[0] = -1;
            int multiply = 1, num = 0;
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    if (mas[i, j] < 0)
                    {
                        multiply = 1;
                        break;

                    }
                    else
                    {
                        multiply *= mas[i, j];
                    }
                    if (j == mas.GetLength(1) - 1)
                    {
                        multiplies[i] = multiply;
                        indexes[num] = i;
                        num++;
                    }
                }
                multiply = 1;

            }
            num = 0;
            Console.WriteLine("\nRows with no negative elements and multiply of their elements: ");

            for (int i = 0; i < multiplies.Length; i++)
            {
                if (indexes[0] == -1)
                {
                    Console.WriteLine("There is no rows with no negative elements");
                    break;
                }
                if (i==indexes[num])
                {
                    num++;
                    Console.WriteLine($"Row index {i} - its multiply = {multiplies[i]}");
                }
                
            }
            Console.WriteLine();
        }
            

        public static void MaxFromSumsOfElementsThatLayOnParrallelToMainDiagonals(int[,] mas)
        {
            int[] sums_of_elements = new int[2 * mas.GetLength(0) - 4];

            int sum = 0, leftindex = 0;
            for (int i = 1; i <= mas.GetLength(0) - 2; i++)
            {
                for (int j = i; j <= mas.GetLength(1) - 1; j++)
                {

                    sum += mas[leftindex, j];
                    leftindex++;
                    if (j == mas.GetLength(1) - 1)
                    {
                        sums_of_elements[i - 1] = sum;
                        
                    }
                }
                sum = 0;
                leftindex = 0;
            }
            for (int i = 1; i <= mas.GetLength(0) - 2; i++)
            {
                for (int j = i; j <= mas.GetLength(1) - 1; j++)
                {

                    sum += mas[j, leftindex];
                    leftindex++;
                    if (j == mas.GetLength(1) - 1)
                    {
                        sums_of_elements[mas.GetLength(1) - 3 + i] = sum;
                        
                    }
                }
                sum = 0;
                leftindex = 0;
            }
            int max = sums_of_elements[0];
            for (int i = 0; i < sums_of_elements.Length; i++)
            {

                if (sums_of_elements[i] > max)
                {
                    max = sums_of_elements[i];
                }
            }
            Console.WriteLine("ONLY FOR SQUARE MATRIX:");
            Console.WriteLine($"Max sum of elements that lay on diagonals parrallel to main diagonal = {max}\n");
        }

        public static void SumOfElementsInColsWithNoNegative(int[,] mas)
        {
            int[] sums = new int[mas.GetLength(1)];
            int[] indexes = new int[mas.GetLength(1)];
            indexes[0] = -1;
            
            int num = 0;
            int sum = 0;
            for(int i = 0; i < mas.GetLength(1); i++)
            {
                for(int j = 0; j < mas.GetLength(0); j++)
                {
                    sum += mas[j, i];
                    if (mas[j, i] < 0)
                    {
                        
                        break;
                    }
                    if(j == mas.GetLength(0) - 1)
                    {
                        sums[i] = sum;
                        indexes[num] = i;
                        num++;
                    }
                    
                }
                
                sum = 0;
            }
            num = 0;
            Console.WriteLine("\nSums of cols with no negative elements: ");
            for(int i = 0; i < sums.Length; i++)
            {
                if (indexes[0] == -1)
                {
                    Console.WriteLine("There is no cols with no negative elements");
                    break;
                }
                if (i == indexes[num])
                {
                    Console.WriteLine($"Sum of col which index is {i} is {sums[i]}");
                    num++;
                    
                }
            }
            
        }
        public static void MinFromSumsoOfAbsElementsThatLayOnParrallelToSecondaryDiagonal(int[,] mas)
        {
            int[] sums_of_elements = new int[2 * mas.GetLength(0) - 4];

            int num = 0, sum = 0, leftindex = 0;
            for (int i = mas.GetLength(0) - 2; i >= 1; i--)
            {
                for (int j = i; j >= 0; j--)
                {

                    sum += Math.Abs(mas[leftindex, j]);
                    leftindex++;
                    if (j == 0)
                    {
                        sums_of_elements[num] = sum;
                        num++;
                    }
                }
                sum = 0;
                leftindex = 0;
            }
            leftindex = mas.GetLength(0) - 1;
            for (int i = 1; i <= mas.GetLength(0) - 2; i++)
            {
                for (int j = i; j <= mas.GetLength(0) - 1; j++)
                {

                    sum += Math.Abs(mas[j, leftindex]);

                    leftindex--;
                    if (j == mas.GetLength(0) - 1)
                    {
                        sums_of_elements[num] = sum;
                        num++;
                    }

                }
                sum = 0;
                leftindex = mas.GetLength(0) - 1;
            }
            int min = sums_of_elements[0];
            for (int i = 0; i < sums_of_elements.Length; i++)
            {
                if (sums_of_elements[i] < min)
                {
                    min = sums_of_elements[i];
                }
            }
            Console.WriteLine("\nONLY FOR SQUARE MATRIX:");
            Console.WriteLine($"Min sum of absolute values of elements that lay on diagonals parrallel to secondary diagonal = {min}\n");
        }
        public static void SumOfElementsInColsWithAtLeastOneNegative(int[,] mas)
        {
            int count = 0, sum = 0;
            int[] sums = new int[mas.GetLength(1)];
            int[] indexes = new int[mas.GetLength(1)];
            indexes[0] = -1;
            int num = 0;
            for (int i = 0; i < mas.GetLength(1); i++)
            {
                for (int j = 0; j < mas.GetLength(0); j++)
                {
                    if (mas[j, i] < 0)
                    {
                        count++;
                    }
                    sum += mas[j, i];
                    if (j == mas.GetLength(0) - 1 && count != 0)
                    {
                        sums[i] = sum;
                        indexes[num] = i;
                        num++;
                    }

                }
                count = 0;
                sum = 0;
            }
            int max = sums[0];
            num = 0;
            Console.WriteLine("\nSums of cols with at least one negative element:");
            for (int i = 0; i < sums.Length; i++)
            {
                if (indexes[0] == -1)
                {
                    Console.WriteLine("There is cols with at least one negative element");
                    break;
                }
                if (i == indexes[num])
                {
                    num++;
                    Console.WriteLine($"Sum of col which index is {i} = {sums[i]}");
                }
                
            }

        }

        public static double Determinant(int[,] mas)
        {
            double det = 0;

            if (mas.GetLength(0) > 2)
            {
                int[,] m = new int[mas.GetLength(0)-1,mas.GetLength(0)-1];
                for (int r = 0; r < mas.GetLength(0); r++)
                {

                    for (int i = 1; i < mas.GetLength(0); i++)
                    {

                        for (int j = 0; j < mas.GetLength(0); j++)
                        {
                            if (j != r && j < r)
                            {
                               m[i - 1, j] = mas[i, j];
                            }
                            else if (j != r && j > r)
                            {
                                m[i - 1, j - 1] = mas[i, j];
                            }

                        }

                    }



                    det += mas[0, r] * Math.Pow(-1, 1 + r + 1) * Determinant(m);


                }


            }
            if (mas.GetLength(0) == 2)
            {
                det = mas[0, 0] * mas[1, 1] - mas[1, 0] * mas[0, 1];
                return det;
            }
            return det;
        }

        public static double[,] GetTransposed <T>( T[,] mas)
        {
            double[,] transp = new double[mas.GetLength(1),mas.GetLength(0)];
            for (int i = 0; i < mas.GetLength(1); i++)
            {
                for (int j = 0; j < mas.GetLength(0); j++)
                {
                    transp[i, j] = Convert.ToDouble(mas[j, i]);
                }
            }

            return transp;
        }

        public static double[,] GetRevert(int[,] mas)
        {
            double det;
            double firstdet;

            int[,] m = new int[mas.GetLength(0) - 1, mas.GetLength(1) - 1];
                double[,] inv = new double[mas.GetLength(0), mas.GetLength(1)];
                firstdet = Determinant(mas);
            
                for (int k = 0; k < mas.GetLength(0); k++)
                {
                    for (int r = 0; r < mas.GetLength(1); r++)
                    {
                        for (int i = 0; i < mas.GetLength(0); i++)
                        {
                            if (i == k) continue;
                            else
                            {
                                for (int j = 0; j < mas.GetLength(0); j++)
                                {
                                    if (j == r) continue;
                                    else
                                    {
                                        if (i < k && j < r) m[i, j] = mas[i, j];
                                        if (i < k && j > r) m[i, j - 1] = mas[i, j];
                                        if (i > k && j < r) m[i - 1, j] = mas[i, j];
                                        if (i > k && j > r) m[i - 1, j - 1] = mas[i, j];
                                    }
                                }
                            }
                        }
                        det = Determinant(m) * Math.Pow(-1, k + 1 + r + 1) / firstdet;
                    
                        inv[k, r] = det;
                    }
                }
                inv = GetTransposed(inv);
                return inv;
            }

        public static void MatrixMultiply(int[,] mas1, int[,] mas2)
        {
            if (mas1.GetLength(1) == mas2.GetLength(0))
            {
                double[,] mas3 = new double[mas1.GetLength(0),mas2.GetLength(1)];
                for (int i = 0; i < mas1.GetLength(0); i++)
                {
                    for (int j = 0; j < mas2.GetLength(1); j++)
                    {
                        for (int k = 0; k < mas1.GetLength(0); k++)
                        {
                            mas3[i, j] += mas1[i, k] * mas2[k, j];
                        }

                    }
                }
                Console.WriteLine("Multiply of 2 matrix: ");
                DisplayDoubleIntMatrix(mas3);
            }
            else throw new Exception("Those matrix cannot be multiplied");
        }
    }
}
