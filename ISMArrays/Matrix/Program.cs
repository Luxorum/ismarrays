﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            int rows, cols,rank,a,b;
            bool flag;
            do
            {
                Console.Write("Input number of rows: ");
                flag = int.TryParse(Console.ReadLine(),out rows );
                if (rows < 0) flag = false;
                if (!flag) Console.WriteLine("Incorrect input!");
            } while (!flag);
            do
            {
                Console.Write("Input number of cols: ");
                flag = int.TryParse(Console.ReadLine(), out cols);
                if (cols < 0) flag = false;
                if (!flag) Console.WriteLine("Incorrect input!");
            } while (!flag);
            do
            {
                Console.Write("Input rank of square matrix: ");
                flag = int.TryParse(Console.ReadLine(), out rank);
                if (rank < 0) flag = false;
                if (!flag) Console.WriteLine("Incorrect input!");
            } while (!flag);
            do
            {
                Console.Write("Input lower random number: ");
                flag = int.TryParse(Console.ReadLine(), out a);
                if (!flag) Console.WriteLine("Incorrect input!");
            } while (!flag);
            do
            {
                Console.Write("Input upper random number: ");
                flag = int.TryParse(Console.ReadLine(), out b);
                if (b < a)
                {
                    Console.WriteLine("Upper random cannot be lower than lower random");
                    flag = false;
                   
                }
                if (!flag) Console.WriteLine("Incorrect input!");
            } while (!flag);
            int[,] arr = new int[rows,cols];
            int[,] square = new int[rank, rank];
            int[,] arr2 = new int[cols, rows];

            FillHyphens();
            Tasks.FillAndDisplayMatrix(arr,a,b);
           
           
            Tasks.CountOfRowsWithoutNulls(arr);
            Tasks.CountOfMaxNumberMoreThanOneTime(arr);
            Tasks.CountOfColsWithAtLeastOneNull(arr);
            Tasks.NumberOfRowWithTheLongestSerie(arr);
            Tasks.MultiplyInRowsWithNoNegativeElements(arr);
            Tasks.SumOfElementsInColsWithAtLeastOneNegative(arr);
            Tasks.SumOfElementsInColsWithNoNegative(arr);
            Console.WriteLine();
            double[,] transposed = Tasks.GetTransposed(arr);
            Console.WriteLine("Your transposed matrix: ");
            Tasks.DisplayDoubleIntMatrix(transposed);
            FillHyphens();

            Tasks.FillAndDisplayMatrix(square, a, b);
            Tasks.MinFromSumsoOfAbsElementsThatLayOnParrallelToSecondaryDiagonal(square);
            Tasks.MaxFromSumsOfElementsThatLayOnParrallelToMainDiagonals(square);
            double determinant = Tasks.Determinant(square);
            Console.WriteLine("\nONLY FOR SQUARE MATRIX: ");
            Console.WriteLine($"Determinant = {determinant}\n");
            
            Console.WriteLine("ONLY FOR SQUARE MATRIX: ");
            double[,] revert = Tasks.GetRevert(square);
            Console.WriteLine("Revert matrix: ");
            Tasks.DisplayMatrix(revert);
            Console.WriteLine();
            Console.WriteLine("First Matrix: ");
            Tasks.FillAndDisplayMatrix(arr,a,b);
            Console.WriteLine("Second Matrix: ");
            Tasks.FillAndDisplayMatrix(arr2,a,b);
            Tasks.MatrixMultiply(arr, arr2);
        }
        static void FillHyphens()
        {
            for (int i = 0; i < 50; i++)
            {
                Console.Write("-");
            }
            Console.WriteLine();
        }
    }
    
}
